
public class Main {

	public static void main(String[] args) {
		Rectangle r = new Rectangle(5, 8, new Point(6, 10));
		System.out.println(r.area());
		System.out.println(r.perimeter());
		
		Point[] pa = r.corners();
		
		for(int i = 0; i < pa.length ; i++) {
			System.out.println(pa[i].xCoord + " "+ pa[i].yCoord);
		}
	}
}
