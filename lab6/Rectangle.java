public class Rectangle {

	int sideA;
	int sideB;
	Point topLeft;
	
	public Rectangle(int sideA, int sideB, Point topLeft) {
		this.sideA = sideA;
		this.sideB = sideB;
		this.topLeft = topLeft;
		
	}
	
	public int area() {
		return this.sideA * this.sideB;
		
	}
	
	public int perimeter() {
		return (this.sideA + this.sideB) * 2;
		
	}
	
	public Point[] corners() {
		Point bottomLeft = new Point(this.topLeft.xCoord, this.topLeft.yCoord - this.sideA);
		Point bottomRight = new Point(this.topLeft.xCoord + this.sideB, this.topLeft.yCoord - this.sideA);
		Point topRight = new Point(this.topLeft.xCoord + this.sideB, this.topLeft.yCoord);
		
		Point[] pointArray = new Point[4];
		pointArray[0] = this.topLeft;
		pointArray[1] = bottomLeft;
		pointArray[2] = topRight;
		pointArray[3] = bottomRight;
		return pointArray;		
		
	}
	
}
