package stack;

import java.util.ArrayList;

public class StackArrayImpl implements Stack{
	ArrayList stack = new ArrayList();
	
	public void push(Object item) {
		stack.add(new StackItem(item));
	}

	public Object pop() {
		return ((StackItem)stack.remove(stack.size()-1)).getItem();
	}

	public boolean empty() {
		return stack.isEmpty();
	}

}
