import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {
	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		boolean control;

		printBoard(board);

		control = true;

		while (control){
			System.out.print("Player 1 enter row number:");
			int row = reader.nextInt();
			System.out.print("Player 1 enter column number:");
			int col = reader.nextInt();

            /*
            Here, i write it for if the area is not empty
            'just write a right please' loop.
            */
			if (!isSuitable(board, row, col)){
				boolean isRight = true;
				while (isRight){
					System.out.println("Please write an empty row and column number");
					System.out.print("Player 1 enter row number:");
					row = reader.nextInt();
					System.out.print("Player 1 enter column number:");
					col = reader.nextInt();

					isRight = !isSuitable(board, row, col);
				}
			}

			board[row - 1][col - 1] = 'X';
			printBoard(board);

			//  i'm controlling the game it ends or continues
			control = func_Control(board);

            /*
              i'm again controlling the game if it ended,
              it just continue end breaking the loop.
              if it is not it will go into this if statement
              and it will continue take an input.
            */
			if (control){
				System.out.print("Player 2 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				col = reader.nextInt();

                /*
                Here, i write it for if the area is not empty
                'just write a right please' loop.
                 */
				if (!isSuitable(board, row, col)){
					boolean isRight = true;
					while (isRight){
						System.out.println("Please write an empty row and column number");
						System.out.print("Player 2 enter row number:");
						row = reader.nextInt();
						System.out.print("Player 2 enter column number:");
						col = reader.nextInt();

						isRight = !isSuitable(board, row, col);
					}
				}

				board[row - 1][col - 1] = 'O';
				printBoard(board);

				//  i'm controlling the game it ends or continues
				control = func_Control(board);
			}
		}
		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");
			}
			System.out.println();
			System.out.println("   -----------");
		}
	}

	/*
    This function is controlling the game
    if it's over, and also return a boolean value.
     */
	private static boolean func_Control(char[][] board){
		int i;
		int j = 1;
		boolean control = true;

		for (i = 0; i < 3; i++){
			if ((board[i][j-1] == board[i][j] && board[i][j-1] == board[i][j+1]) && (board[i][j-1] == 'X' || board[i][j-1] == 'O')){
				System.out.println("Winner is: " + board[i][j-1]);
				control = false;
			}
			else if ((board[j-1][i] == board[j][i] && board[j-1][i] == board[j+1][i]) && (board[j-1][i] == 'X' || board[j-1][i] == 'O')){
				System.out.println("Winner is: " + board[j-1][i]);
				control = false;
			}

            /*
            here, i seperate cross alternatives
             */
			if (i == 1){
				if ((board[i][j] == board[i-1][j-1] && board[i][j] == board[i+1][j+1]) && (board[i][j] == 'X' || board[i][j] == 'O')){
					System.out.println("Winner is: " + board[i][j]);
					control = false;
				}
			}
			if(i == 2){
				if ((board[i-1][j] == board[i-2][j+1] && board[i-1][j] == board[i][j-1]) && (board[i][j] == 'X' || board[i][j] == 'O')){
					System.out.println("Winner is: " + board[i-1][j]);
					control = false;
				}
			}
		}
		return control;
	}

	/*
    This function is controlling the area is empty or not
     */
	private static boolean isSuitable(char[][] board, int row, int column){
		boolean control = true;

		if (board[row-1][column-1] != ' '){
			control = false;
			return control;
		}
		else{
			return control;
		}
	}
}
